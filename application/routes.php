<?php
  //загрузка роутера
$router = Router::getInstance();
  //список роутов
$router->route('', 'base');
  //LoginController
$router->route('login', 'login', 'get');
$router->route('login', 'login/login', 'post');
$router->route('logout', 'login/logout');
  //RegistrationController
$router->route('registration', 'registration', 'get');
$router->route('registration', 'registration/register', 'post');
$router->route('user', 'user');
  //api user
$router->route('api/login', 'login/api_login', 'post');
$router->route('api/logout', 'login/api_logout', 'post');
$router->route('api/registration', 'registration/api_registration', 'post');
$router->route('api/user', 'user/api_user', 'post');
  //api cycle
$router->route('api/cycle', 'cycle/api_cycle', 'post');
$router->route('api/removecycle', 'cycle/api_removecycle', 'post');
$router->route('api/addcycle', 'cycle/api_addcycle', 'post');
$router->route('api/updatecycle', 'cycle/api_updatecycle', 'post');
  //api discipline
$router->route('api/discipline', 'discipline/api_discipline', 'post');
$router->route('api/removediscipline', 'discipline/api_removediscipline', 'post');
$router->route('api/adddiscipline', 'discipline/api_adddiscipline', 'post');
$router->route('api/updatediscipline', 'discipline/api_updatediscipline', 'post');

