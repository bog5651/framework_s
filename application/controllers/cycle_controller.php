<?php
class CycleController extends Controller
{
    public function index()
    {
    }

  //get cycles or cycle by id
    public function api_cycle()
    {
        $data = $this->apiData(true, true);
        if (empty($data) || !property_exists($data, 'cycle')) {
            echo json_encode([
                'success' => 0,
                'error' => [
                    'code' => 104,
                    'message' => 'Wrong data set. Do not fount \"cycle\"'
                ]
            ]);
            die();
        }

        $session_model = $this->loader->getModel('session');
        $user_id = $session_model->authentication($data->token);
        if ($user_id < 0) {
            echo json_encode([
                'success' => 0,
                'error' => [
                    'code' => 201,
                    'message' => 'Wrong token'
                ]
            ]);
            die();
        }
        $cycle_model = $this->loader->getModel('cycle');
        if (property_exists($data->cycle, "id")) {
            $cycles = $cycle_model->getCycleById($data->cycle->id);
            if ($cycles != null) {
                echo json_encode([
                    'success' => 1,
                    'cycles' => [$cycles]
                ]);
                die();
            } else {
                echo json_encode([
                    'success' => 0,
                    'error' => [
                        'code' => 201,
                        'message' => 'Wrong id'
                    ]
                ]);
                die();
            }
        }
        $cycles = $cycle_model->getCycles();
        echo json_encode([
            'success' => 1,
            'cycles' => $cycles
        ]);
        die();
    }

  //get remove cycle by id
    public function api_removecycle()
    {
        $data = $this->apiData(true, true);
        if (empty($data) || !property_exists($data, 'cycle')) {
            echo json_encode([
                'success' => 0,
                'error' => [
                    'code' => 104,
                    'message' => 'Wrong data set. Do not fount \"cycle\"'
                ]
            ]);
            die();
        }

        $session_model = $this->loader->getModel('session');
        $user_id = $session_model->authentication($data->token);
        if ($user_id < 0) {
            echo json_encode([
                'success' => 0,
                'error' => [
                    'code' => 201,
                    'message' => 'Wrong token'
                ]
            ]);
            die();
        }
        $cycle_model = $this->loader->getModel('cycle');
        if (property_exists($data->cycle, "id")) {
            $cycles = $cycle_model->deleteCycle($data->cycle->id);
            if ($cycles) {
                echo json_encode([
                    'success' => 1
                ]);
                die();
            } else {
                echo json_encode([
                    'success' => 0,
                    'error' => [
                        'code' => 104,
                        'message' => 'Inner Error'
                    ]
                ]);
                die();
            }
        }
        echo json_encode([
            'success' => 0,
            'error' => [
                'code' => 104,
                'message' => 'Wrong data set. Do not fount \"id\"'
            ]
        ]);
        die();
    }

  //get add cycle by id
    public function api_addcycle()
    {
        $data = $this->apiData(true, true);
        if (empty($data) || !property_exists($data, 'cycle')) {
            echo json_encode([
                'success' => 0,
                'error' => [
                    'code' => 104,
                    'message' => 'Wrong data set. Do not fount \"cycle\"'
                ]
            ]);
            die();
        }

        $session_model = $this->loader->getModel('session');
        $user_id = $session_model->authentication($data->token);
        if ($user_id < 0) {
            echo json_encode([
                'success' => 0,
                'error' => [
                    'code' => 201,
                    'message' => 'Wrong token'
                ]
            ]);
            die();
        }
        $cycle_model = $this->loader->getModel('cycle');
        if (property_exists($data->cycle, "cycle_name")) {
            $id = $cycle_model->addCycle($data->cycle->cycle_name);
            if ($id) {
                echo json_encode([
                    'success' => 1,
                    'cycle_id' => $id
                ]);
                die();
            } else {
                echo json_encode([
                    'success' => 0,
                    'error' => [
                        'code' => 104,
                        'message' => 'Inner Error'
                    ]
                ]);
                die();
            }
        }
        echo json_encode([
            'success' => 0,
            'error' => [
                'code' => 104,
                'message' => 'Wrong data set. Do not fount \"cycle_name\"'
            ]
        ]);
        die();
    }

    //get add cycle by id
    public function api_updatecycle()
    {
        $data = $this->apiData(true, true);
        if (empty($data) || !property_exists($data, 'cycle')) {
            echo json_encode([
                'success' => 0,
                'error' => [
                    'code' => 104,
                    'message' => 'Wrong data set. Do not fount \"cycle\"'
                ]
            ]);
            die();
        }

        $session_model = $this->loader->getModel('session');
        $user_id = $session_model->authentication($data->token);
        if ($user_id < 0) {
            echo json_encode([
                'success' => 0,
                'error' => [
                    'code' => 201,
                    'message' => 'Wrong token'
                ]
            ]);
            die();
        }
        $cycle_model = $this->loader->getModel('cycle');
        if (property_exists($data->cycle, "cycle_name") &&
            property_exists($data->cycle, "id")) {

            $cycle = $cycle_model->getCycleById($data->cycle->id);
            if ($cycle != null) {
                $row = $cycle_model->updateCycle($data->cycle->id, $data->cycle->cycle_name);
                if ($row) {
                    echo json_encode([
                        'success' => 1
                    ]);
                    die();
                } else {
                    echo json_encode([
                        'success' => 0,
                        'error' => [
                            'code' => 104,
                            'message' => 'Inner Error'
                        ]
                    ]);
                    die();
                }
            } else {
                echo json_encode([
                    'success' => 0,
                    'error' => [
                        'code' => 201,
                        'message' => 'Wrong id'
                    ]
                ]);
                die();
            }
        }
        echo json_encode([
            'success' => 0,
            'error' => [
                'code' => 104,
                'message' => 'Wrong data set. Do not fount \"cycle_name\"'
            ]
        ]);
        die();
    }
}