<?php
class DisciplineController extends Controller
{
    public function index()
    {
    }

  //get disciplines or discipline by id
    public function api_discipline()
    {
        $data = $this->apiData(true, true);
        if (empty($data) || !property_exists($data, 'discipline')) {
            echo json_encode([
                'success' => 0,
                'error' => [
                    'code' => 104,
                    'message' => 'Wrong data set. Do not fount \"discipline\"'
                ]
            ]);
            die();
        }

        $session_model = $this->loader->getModel('session');
        $user_id = $session_model->authentication($data->token);
        if ($user_id < 0) {
            echo json_encode([
                'success' => 0,
                'error' => [
                    'code' => 201,
                    'message' => 'Wrong token'
                ]
            ]);
            die();
        }
        $discipline_model = $this->loader->getModel('discipline');
        if (property_exists($data->discipline, "id")) {
            $disciplines = $discipline_model->getDisciplineById($data->discipline->id);
            if ($disciplines != null) {
                echo json_encode([
                    'success' => 1,
                    'disciplines' => [$disciplines]
                ]);
                die();
            } else {
                echo json_encode([
                    'success' => 0,
                    'error' => [
                        'code' => 201,
                        'message' => 'Wrong id'
                    ]
                ]);
                die();
            }
        }
        $disciplines = $discipline_model->getDisciplines();
        echo json_encode([
            'success' => 1,
            'disciplines' => $disciplines
        ]);
        die();
    }

  //get remove discipline by id
    public function api_removediscipline()
    {
        $data = $this->apiData(true, true);
        if (empty($data) || !property_exists($data, 'discipline')) {
            echo json_encode([
                'success' => 0,
                'error' => [
                    'code' => 104,
                    'message' => 'Wrong data set. Do not fount \"discipline\"'
                ]
            ]);
            die();
        }

        $session_model = $this->loader->getModel('session');
        $user_id = $session_model->authentication($data->token);
        if ($user_id < 0) {
            echo json_encode([
                'success' => 0,
                'error' => [
                    'code' => 201,
                    'message' => 'Wrong token'
                ]
            ]);
            die();
        }
        $discipline_model = $this->loader->getModel('discipline');
        if (property_exists($data->discipline, "id")) {
            $disciplines = $discipline_model->deleteDiscipline($data->discipline->id);
            if ($disciplines) {
                echo json_encode([
                    'success' => 1
                ]);
                die();
            } else {
                echo json_encode([
                    'success' => 0,
                    'error' => [
                        'code' => 104,
                        'message' => 'Inner Error'
                    ]
                ]);
                die();
            }
        }
        echo json_encode([
            'success' => 0,
            'error' => [
                'code' => 104,
                'message' => 'Wrong data set. Do not fount \"id\"'
            ]
        ]);
        die();
    }

  //get add discipline by id
    public function api_adddiscipline()
    {
        $data = $this->apiData(true, true);
        if (empty($data) || !property_exists($data, 'discipline')) {
            echo json_encode([
                'success' => 0,
                'error' => [
                    'code' => 104,
                    'message' => 'Wrong data set. Do not fount \"discipline\"'
                ]
            ]);
            die();
        }

        $session_model = $this->loader->getModel('session');
        $user_id = $session_model->authentication($data->token);
        if ($user_id < 0) {
            echo json_encode([
                'success' => 0,
                'error' => [
                    'code' => 201,
                    'message' => 'Wrong token'
                ]
            ]);
            die();
        }
        $discipline_model = $this->loader->getModel('discipline');
        if (property_exists($data->discipline, "d_name")
            && property_exists($data->discipline, "semester")
            && property_exists($data->discipline, "school_hours")
            && property_exists($data->discipline, "d_cycle_ID")) {
            $id = $discipline_model->addDiscipline($data->discipline->d_name, $data->discipline->semester, $data->discipline->school_hours, $data->discipline->d_cycle_ID);
            if ($id > 0) {
                echo json_encode([
                    'success' => 1,
                    'discipline_id' => $id
                ]);
                die();
            } else {
                echo json_encode([
                    'success' => 0,
                    'error' => [
                        'code' => 104,
                        'message' => 'Wrond d_cycle_ID'
                    ]
                ]);
                die();
            }
        }
        echo json_encode([
            'success' => 0,
            'error' => [
                'code' => 104,
                'message' => 'Wrong data set. Do not found some param.'
            ]
        ]);
        die();
    }

    //get add discipline by id
    public function api_updatediscipline()
    {
        $data = $this->apiData(true, true);
        if (empty($data) || !property_exists($data, 'discipline')) {
            echo json_encode([
                'success' => 0,
                'error' => [
                    'code' => 104,
                    'message' => 'Wrong data set. Do not fount \"discipline\"'
                ]
            ]);
            die();
        }

        $session_model = $this->loader->getModel('session');
        $user_id = $session_model->authentication($data->token);
        if ($user_id < 0) {
            echo json_encode([
                'success' => 0,
                'error' => [
                    'code' => 201,
                    'message' => 'Wrong token'
                ]
            ]);
            die();
        }
        $discipline_model = $this->loader->getModel('discipline');
        if (property_exists($data->discipline, "id")
            && property_exists($data->discipline, "d_name")
            && property_exists($data->discipline, "semester")
            && property_exists($data->discipline, "school_hours")
            && property_exists($data->discipline, "d_cycle_ID")) {

            $discipline = $discipline_model->getDisciplineById($data->discipline->id);
            if ($discipline != null) {
                $row = $discipline_model->updateDiscipline($data->discipline->id, $data->discipline->d_name, $data->discipline->semester, $data->discipline->school_hours, $data->discipline->d_cycle_ID);
                if ($row) {
                    echo json_encode([
                        'success' => 1
                    ]);
                    die();
                } else {
                    echo json_encode([
                        'success' => 0,
                        'error' => [
                            'code' => 104,
                            'message' => 'Wrond d_cycle_ID'
                        ]
                    ]);
                    die();
                }
            } else {
                echo json_encode([
                    'success' => 0,
                    'error' => [
                        'code' => 201,
                        'message' => 'Wrong id'
                    ]
                ]);
                die();
            }
        }
        echo json_encode([
            'success' => 0,
            'error' => [
                'code' => 104,
                'message' => 'Wrong data set. Do not fount \"cycle_name\"'
            ]
        ]);
        die();
    }
}