<?php
class DisciplineModel extends Model
{
    public function getDisciplineById($id)
    {
        $ret = null;
        $res = $this->db->query('SELECT * FROM Discipline WHERE discipline_ID = ?', [$id]);
        if (!empty($res)) {
            $ret = $res[0];
        }
        return $ret;
    }

    public function getDisciplines()
    {
        return $this->db->query('SELECT * FROM Discipline');
    }

    public function addDiscipline(string $d_name, string $semester, string $school_hours, string $d_cycle_ID) : int
    {
        $row = $this->db->query('SELECT * from Cycle where cycle_ID = ?', [$d_cycle_ID]);
        if (!empty($row)) {
            $res = $this->db->query(
                'INSERT INTO Discipline (d_name, semester, school_hours, d_cycle_ID) VALUES (?, ?, ?, ?)',
                [$d_name, $semester, $school_hours, $d_cycle_ID]
            );
            if ($res) {
                return $this->db->getlastId();
            }
            return -1;
        }
        return -2;
    }

    public function updateDiscipline(string $id, string $d_name, string $semester, string $school_hours, string $d_cycle_ID)
    {
        $row = $this->db->query('SELECT * from Cycle where cycle_ID = ?', [$d_cycle_ID]);
        if (!empty($row)) {
            return $this->db->query(
                'UPDATE Discipline SET d_name = ?, semester = ?, school_hours = ?, d_cycle_ID = ? WHERE discipline_ID = ?',
                [$d_name, $semester, $school_hours, $d_cycle_ID, $id]
            );
        }
        return false;
    }

    public function deleteDiscipline(int $id)
    {
        return $this->db->query('DELETE FROM Discipline WHERE discipline_ID = ?', [$id]);
    }
}

