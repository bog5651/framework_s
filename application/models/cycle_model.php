<?php
class CycleModel extends Model
{
    public function getCycleById($id)
    {
        $ret = null;
        $res = $this->db->query('SELECT cycle_name FROM Cycle WHERE cycle_ID = ?', [$id]);
        if (!empty($res)) {
            $ret = $res[0];
        }
        return $ret;
    }

    public function getCycles()
    {
        return $this->db->query('SELECT * FROM Cycle');
    }

    public function addCycle(string $cycle_name) : int
    {
        $res = $this->db->query('INSERT INTO Cycle (cycle_name) VALUES (?)', [$cycle_name]);
        if ($res) {
            return $this->db->getlastId();
        }
        return -1;
    }

    public function updateCycle(string $id, string $cycle_name)
    {
        return $this->db->query('UPDATE Cycle SET cycle_name = ? WHERE cycle_ID = ?  ', [$cycle_name, $id]);
    }

    public function deleteCycle(int $id)
    {
        return $this->db->query('DELETE FROM Cycle WHERE cycle_ID = ?', [$id]);
    }
}

