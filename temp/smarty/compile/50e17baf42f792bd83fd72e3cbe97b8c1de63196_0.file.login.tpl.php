<?php
/* Smarty version 3.1.33, created on 2018-12-21 15:40:37
  from '/var/www/html/application/views/pages/login.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5c1d0975896ef9_70310072',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '50e17baf42f792bd83fd72e3cbe97b8c1de63196' => 
    array (
      0 => '/var/www/html/application/views/pages/login.tpl',
      1 => 1545403780,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5c1d0975896ef9_70310072 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_loadInheritance();
$_smarty_tpl->inheritance->init($_smarty_tpl, true);
?>

<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_8724071185c1d09757d4f23_24970382', "content");
$_smarty_tpl->inheritance->endChild($_smarty_tpl, "./page.tpl");
}
/* {block "content"} */
class Block_8724071185c1d09757d4f23_24970382 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'content' => 
  array (
    0 => 'Block_8724071185c1d09757d4f23_24970382',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

  <div class="row justify-content-center my-3">
    <div class="col col-6">
      <div class="card border-dark">
        <div class="card-header">
          <h3>Login</h3>
        </div>
        <div class="card-body">
          <?php if (!empty($_smarty_tpl->tpl_vars['messages']->value)) {?>
            <div class="form-group">
              <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['messages']->value, 'message');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['message']->value) {
?>
                <div class="alert alert-danger"><i class="fa fa-warning"></i> <?php echo $_smarty_tpl->tpl_vars['message']->value;?>
</div>
              <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
            </div>
          <?php }?>
          <form method="POST" action="http://<?php echo $_smarty_tpl->tpl_vars['base_url']->value;?>
login">
            <div class="form-group">
              <label>Login</label>
              <input class="form-control" type="text" name="login" placeholder="Login or e-mail">
            </div>
            <div class="form-group">
              <label>Password</label>
              <input class="form-control" type="password" name="password" placeholder="Password">
            </div>
            <hr class="border-secondary">
            <div class="form-group">
              <input class="btn btn-block btn-dark" type="submit" value="Login">
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>  
<?php
}
}
/* {/block "content"} */
}
