<?php
/* Smarty version 3.1.33, created on 2018-12-21 15:40:39
  from '/var/www/html/application/views/snippets/lists.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5c1d0977c54232_95406544',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'bab8c913c5f6a06a8918aec1f747f35b79e9378b' => 
    array (
      0 => '/var/www/html/application/views/snippets/lists.tpl',
      1 => 1545403780,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5c1d0977c54232_95406544 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->smarty->ext->_tplFunction->registerTplFunctions($_smarty_tpl, array (
  'list' => 
  array (
    'compiled_filepath' => '/var/www/html/temp/smarty/compile/bab8c913c5f6a06a8918aec1f747f35b79e9378b_0.file.lists.tpl.php',
    'uid' => 'bab8c913c5f6a06a8918aec1f747f35b79e9378b',
    'call_name' => 'smarty_template_function_list_18169150665c1d0977c02c08_60637920',
  ),
));
}
/* smarty_template_function_list_18169150665c1d0977c02c08_60637920 */
if (!function_exists('smarty_template_function_list_18169150665c1d0977c02c08_60637920')) {
function smarty_template_function_list_18169150665c1d0977c02c08_60637920(Smarty_Internal_Template $_smarty_tpl,$params) {
$params = array_merge(array('items'=>array()), $params);
foreach ($params as $key => $value) {
$_smarty_tpl->tpl_vars[$key] = new Smarty_Variable($value, $_smarty_tpl->isRenderingCache);
}
?>

  <?php if (!empty($_smarty_tpl->tpl_vars['items']->value)) {?>
    <ul>
      <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['items']->value, 'item');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['item']->value) {
?>
        <li><?php echo $_smarty_tpl->tpl_vars['item']->value;?>
</li>
      <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
    </ul>  
  <?php } else { ?>
    <h4>Empty list</h4>
  <?php }
}}
/*/ smarty_template_function_list_18169150665c1d0977c02c08_60637920 */
}
