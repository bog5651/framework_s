<?php
/* Smarty version 3.1.33, created on 2018-12-21 15:40:45
  from '/var/www/html/application/views/pages/registration.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5c1d097d04aa33_85295680',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '9f8d4d4d897c62c4b3af7c3fe6e319867df60e2e' => 
    array (
      0 => '/var/www/html/application/views/pages/registration.tpl',
      1 => 1545403780,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5c1d097d04aa33_85295680 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_loadInheritance();
$_smarty_tpl->inheritance->init($_smarty_tpl, true);
?>

<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_9014430085c1d097d016d81_87431428', "content");
$_smarty_tpl->inheritance->endChild($_smarty_tpl, "./page.tpl");
}
/* {block "content"} */
class Block_9014430085c1d097d016d81_87431428 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'content' => 
  array (
    0 => 'Block_9014430085c1d097d016d81_87431428',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

  <div class="row justify-content-center">
    <div class="col-6">
      <div class="card card-info my-3 border-dark">
        <div class="card-header">
          <h3>Registration</h3>
        </div>
        <div class="card-body">
          <form method="POST" action="http://<?php echo $_smarty_tpl->tpl_vars['base_url']->value;?>
registration">
            <?php if (!empty($_smarty_tpl->tpl_vars['messages']->value)) {?>
              <div class="form-group">
                <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['messages']->value, 'message');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['message']->value) {
?>
                  <div class="alert alert-danger"><i class="fa fa-warning"></i> <?php echo $_smarty_tpl->tpl_vars['message']->value;?>
</div>
                <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
              </div>
            <?php }?>
            <div class="form-group">
              <label>Login</label>
              <input class="form-control" type="text" name="login" placeholder="Login">
            </div>
            <div class="form-group">
              <label>E-mail</label>
              <input class="form-control" type="text" name="email" placeholder="E-mail">
            </div>
            <div class="form-group">
              <label>Password</label>
              <input class="form-control" type="password" name="password" placeholder="Password">
            </div>
            <div class="form-group">
              <label>Confirm password</label>
              <input class="form-control" type="password" name="confirm" placeholder="Confirm password">
            </div>
            <hr class="border-secondary">
            <div class="form-group">
              <input class="btn btn-block btn-dark" type="submit" value="Registration">
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
<?php
}
}
/* {/block "content"} */
}
