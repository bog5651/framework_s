<?php
/* Smarty version 3.1.33, created on 2018-12-21 15:40:39
  from '/var/www/html/application/views/snippets/nav.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5c1d0977cd2be1_77331460',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '48de3faa8e8d22db317258291cb0d1f5c6c92aa2' => 
    array (
      0 => '/var/www/html/application/views/snippets/nav.tpl',
      1 => 1545403780,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5c1d0977cd2be1_77331460 (Smarty_Internal_Template $_smarty_tpl) {
if (!isset($_smarty_tpl->tpl_vars['login_flag']->value)) {?>
  <?php ob_start();
echo false;
$_prefixVariable1=ob_get_clean();
$_smarty_tpl->_assignInScope('login_flag', $_prefixVariable1);
}?>
<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
  <a class="navbar-brand" href="#">Web OmSTU</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarNav">
    <ul class="nav navbar-nav justify-content-end">
      <?php if ((bool)$_smarty_tpl->tpl_vars['login_flag']->value) {?>
        <li class="nav-item"><a class="nav-link" href="http://<?php echo $_smarty_tpl->tpl_vars['base_url']->value;?>
logout">Logout</a></li>
      <?php } else { ?>
        <li class="nav-item"><a class="nav-link" href="http://<?php echo $_smarty_tpl->tpl_vars['base_url']->value;?>
login">Login</a></li>
        <li class="nav-item"><a class="nav-link" href="http://<?php echo $_smarty_tpl->tpl_vars['base_url']->value;?>
registration">Registration</a></li>
      <?php }?>
    </ul>
  </div>
</nav><?php }
}
