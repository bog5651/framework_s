<?php
/* Smarty version 3.1.33, created on 2018-12-21 15:40:39
  from '/var/www/html/application/views/pages/page.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5c1d0977bda7d7_89919823',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'fbd12fa1509462cabd17311a3d1dd08b5b340831' => 
    array (
      0 => '/var/www/html/application/views/pages/page.tpl',
      1 => 1545403780,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:../snippets/lists.tpl' => 1,
    'file:../snippets/nav.tpl' => 1,
  ),
),false)) {
function content_5c1d0977bda7d7_89919823 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_loadInheritance();
$_smarty_tpl->inheritance->init($_smarty_tpl, false);
$_smarty_tpl->_subTemplateRender("file:../snippets/lists.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

<html>
  <head>
      <link href="http://<?php echo $_smarty_tpl->tpl_vars['base_url']->value;?>
res/css/bootstrap.min.css" rel="stylesheet">
      <link href="http://<?php echo $_smarty_tpl->tpl_vars['base_url']->value;?>
res/css/font-awesome.min.css" rel="stylesheet">
      <?php echo '<script'; ?>
 src="http://<?php echo $_smarty_tpl->tpl_vars['base_url']->value;?>
res/js/jquery-3.3.1.min.js"><?php echo '</script'; ?>
>
      <?php echo '<script'; ?>
 src="http://<?php echo $_smarty_tpl->tpl_vars['base_url']->value;?>
res/js/bootstrap.min.js"><?php echo '</script'; ?>
>
      <title>
          <?php echo $_smarty_tpl->tpl_vars['title']->value;?>

      </title>
  </head>
  <body>
    <?php $_smarty_tpl->_subTemplateRender("file:../snippets/nav.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>
    <div class="container">
      <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_5794687585c1d0977bd9048_44177004', "content");
?>

    </div>
  </body>
</html><?php }
/* {block "content"} */
class Block_5794687585c1d0977bd9048_44177004 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'content' => 
  array (
    0 => 'Block_5794687585c1d0977bd9048_44177004',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

      <?php
}
}
/* {/block "content"} */
}
