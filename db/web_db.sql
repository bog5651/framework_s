SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";
--
CREATE DATABASE IF NOT EXISTS `db` DEFAULT CHARACTER SET utf8 COLLATE 'utf8_general_ci';
USE `db`;

DROP TABLE IF EXISTS `sessions`;
CREATE TABLE IF NOT EXISTS `sessions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `token` varchar(50) NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `date` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `token` (`token`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `firstname` varchar(50) DEFAULT '',
  `lastname` varchar(50) DEFAULT '',
  `birthdate` datetime DEFAULT NULL,
  `login` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `create_date` datetime NOT NULL,
  `update_date` datetime NOT NULL,
  `active` int(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `login` (`login`),
  UNIQUE KEY `email` (`email`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

CREATE TABLE Cycle
	(
	 cycle_ID INT NOT NULL AUTO_INCREMENT,
	 cycle_name VARCHAR(50) NOT NULL,
	 CONSTRAINT PK_Cycle PRIMARY KEY (cycle_ID)
	 )ENGINE=InnoDB  DEFAULT CHARSET=utf8;
CREATE TABLE Discipline
	(
	 discipline_ID INT NOT NULL AUTO_INCREMENT,
	 d_name VARCHAR(50) NOT NULL,
	 semester INT NOT NULL,
	 school_hours INT NOT NULL,
	 d_cycle_ID INT NOT NULL,
	 CONSTRAINT PK_Discipline PRIMARY KEY (discipline_ID)
	)ENGINE=InnoDB  DEFAULT CHARSET=utf8;
CREATE TABLE Specialty
	(
	 specialty_ID INT NOT NULL AUTO_INCREMENT,
	 s_name VARCHAR(50) NOT NULL,
	 s_discipline_ID INT NOT NULL,
	 CONSTRAINT PK_Specialty PRIMARY KEY (specialty_ID)
	 )ENGINE=InnoDB  DEFAULT CHARSET=utf8;
CREATE TABLE Lessons
	(
	lessons_ID INT NOT NULL AUTO_INCREMENT,
	les_name VARCHAR(50) NOT NULL,
	type_of_lessons VARCHAR(50) NOT NULL,
	exam VARCHAR(50) NOT NULL,
	number_of_hours INT NOT NULL,
	l_discipline_ID INT NOT NULL,
	CONSTRAINT PK_Lessons PRIMARY KEY (lessons_ID)
	 )ENGINE=InnoDB  DEFAULT CHARSET=utf8;
